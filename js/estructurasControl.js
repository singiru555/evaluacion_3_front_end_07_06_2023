//a.	Escribe una función llamada esPrimo que tome un número como parámetro y devuelva true si es primo y false si no lo es. 
function esPrimo(numero){
    for(let i=2;numero;i++){
        if(numero%i===0){
            
            return false;
        }
        else{

            return true;
        }
    };
};
//console.log(esPrimo(3)); // Comprobar que funciona

//b.	Utilizando un bucle do-while, solicita al usuario que adivine un número y muestra un mensaje indicando si es demasiado alto, demasiado bajo o correcto.

const numeroSecreto = Math.floor(Math.random()*20) + 1 ;
// console.log(numeroSecreto);
//let intentos = 0;
let numeroUsuario;

do
{
    numeroUsuario = prompt("Ingresa un número del 1 al 20: ");       
    if (isNaN(numeroUsuario)){
        alert("No es un número")
    }
    else{
        if (numeroSecreto == numeroUsuario){
            alert("¡Has acertado!");
            //alert("El número de intentos fue :" + intentos)
        }else if (numeroSecreto > numeroUsuario){
            alert("¡El número ingresado es demasiado bajo!")
            //intentos++
        }else{
            alert("¡El número ingresado es demasiado alto!")
            //intentos++
        }
    }
}while (numeroSecreto !=numeroUsuario);
