//a.	Crea un arreglo de objetos llamado libros donde cada objeto represente un libro con propiedades como título, autor y año de publicación. 
let libros =[{"titulo":"Harry Potter", "autor":"J.K.Rowling", "anoPublicacion":1997},
{"titulo":"Déjà Dead", "autor":"Kathy Reichs", "anoPublicacion":1997},
{"titulo":"Thrawn", "autor":"Timothy Zahn", "anoPublicacion":2017}
];
//console.log(libros);//comprobar salida

//b.	Utilizando el método filter(), crea un nuevo arreglo llamado librosRecientes que contenga solo los libros publicados en los últimos 5 años.


let librosInventados =[
    {"titulo":"libro1", "autor":"J.K.Rowling", "anoPublicacion":2020},
    {"titulo":"libro2", "autor":"J.K.Rowling", "anoPublicacion":2021},
    {"titulo":"libro3", "autor":"J.K.Rowling", "anoPublicacion":2003},
    {"titulo":"libro4", "autor":"J.K.Rowling", "anoPublicacion":1997},
    {"titulo":"libro5", "autor":"J.K.Rowling", "anoPublicacion":1995},
    {"titulo":"libro6", "autor":"J.K.Rowling", "anoPublicacion":2007},
    {"titulo":"libro7", "autor":"J.K.Rowling", "anoPublicacion":2019}
];




let librosRecientes = librosInventados.filter(function(libro){
    return new Date().getFullYear()- libro.anoPublicacion <=5 ; // calcula el año actual y le descuenta el año de publicación, creando un arreglo con solo aquellos títules que sean 5 o menos años desde su publicación
});
console.log(librosRecientes);