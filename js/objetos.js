


    
//a.	Crea un objeto llamado rectangulo con propiedades base y altura. Agrega un método calcularArea que calcule y devuelva el área del rectángulo. 

class Rectangulo {
    constructor(base,altura) {
        this.base = base;
        this.altura=  altura;
        this.calcularArea= function(){
            return this.base*this.altura;
        };
  };
};
class Cuadrado extends Rectangulo {
    constructor(base,altura) {
      super(base,altura);
      this.base =base;
      this.altura= altura;
      this.calcularArea= function(){
        return this.base*2;
    };
  }
};
// //b.	Crea otro objeto llamado cuadrado que herede del objeto rectangulo y sobrescribe el método calcularArea para que calcule el área del cuadrado.
// function Cuadrado(Rectangulo){
cuadrado = new Cuadrado(2); 
console.log(cuadrado.calcularArea(2));